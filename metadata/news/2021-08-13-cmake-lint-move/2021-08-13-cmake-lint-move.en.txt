Title: app-editors/cmake-lint has been renamed to dev-util/cmake-lint
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: <creation date>
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-editors/cmake-lint

Please install dev-util/cmake-lint and *afterwards* uninstall app-editors/cmake-lint.

1. Take note of any packages depending on <old package name>:
cave resolve \!app-editors/cmake-lint

2. Install <new package name>:
cave resolve dev-util/cmake-lint -x

3. Re-install the packages from step 1.

4. Uninstall app-editors/cmake-lint
cave resolve \!app-editors/cmake-lint -x

Do it in *this* order or you'll potentially *break* your system.
