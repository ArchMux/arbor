# Copyright 2022 Timo Gurr <tgurrexherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=oneapi-src project=oneTBB tag=v${PV} ] \
    cmake

SUMMARY="oneAPI Threading Building Blocks (oneTBB)"
DESCRIPTION="
oneTBB is a flexible C++ library that simplifies the work of adding parallelism to complex
applications, even if you are not a threading expert.

The library lets you easily write parallel programs that take full advantage of the multi-core
performance. Such programs are portable, composable and have a future-proof scalability. oneTBB
provides you with functions, interfaces, and classes to parallelize and scale the code. All you
have to do is to use the templates.

The library differs from typical threading packages in the following ways:

- oneTBB enables you to specify logical parallelism instead of threads.
- oneTBB targets threading for performance.
- oneTBB is compatible with other threading packages.
- oneTBB emphasizes scalable, data parallel programming.
- oneTBB relies on generic programming.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# 17 tests failed out of 136, last checked: 2021.7.0
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/hwloc[>=1.11]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DTBB4PY_BUILD:BOOL=FALSE
    -DTBBMALLOC_BUILD:BOOL=TRUE
    -DTBBMALLOC_PROXY_BUILD:BOOL=TRUE
    -DTBB_BUILD:BOOL=TRUE
    -DTBB_CPF:BOOL=FALSE
    -DTBB_DISABLE_HWLOC_AUTOMATIC_SEARCH:BOOL=FALSE
    -DTBB_ENABLE_IPO:BOOL=TRUE
    -DTBB_EXAMPLES:BOOL=FALSE
    -DTBB_FIND_PACKAGE:BOOL=FALSE
    -DTBB_INSTALL_VARS:BOOL=FALSE
    -DTBB_STRICT:BOOL=FALSE
    -DTBB_TEST_SPEC:BOOL=FALSE
    -DTBB_VALGRIND_MEMCHECK:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DTBB_TEST:BOOL=TRUE -DTBB_TEST:BOOL=FALSE'
)

