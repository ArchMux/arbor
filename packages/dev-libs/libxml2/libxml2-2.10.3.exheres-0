# Copyright 2007-2008 Bo Ørsted Andresen
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="XML C Parser and Toolkit v2"
HOMEPAGE="https://gitlab.gnome.org/GNOME/${PN}/-/wikis/home"

BASE_URI="https://www.w3.org/XML"
SCHEMA_TEST_URI="${BASE_URI}/2004/xml-schema-test-suite/xmlschema"
TEST_VERSION="20130923"
TAR1="2002-01-16"
TAR2="2004-01-14"

DOWNLOADS+="
    ${BASE_URI}/Test/xmlts${TEST_VERSION}.tar
    ${SCHEMA_TEST_URI}${TAR1}/xsts-${TAR1}.tar.gz
    ${SCHEMA_TEST_URI}${TAR2}/xsts-${TAR2}.tar.gz"

LICENCES="MIT"
SLOT="2.0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    examples
    ftp [[ description = [ Support for the built-in FTP client (deprecated) ] ]]
"

DEPENDENCIES="
    build+run:
        app-arch/xz
        dev-libs/icu:=
        sys-libs/readline:=
        sys-libs/zlib[>=1.2.5-r1]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --enable-ipv6
    --with-history
    # ICU is required for chromium
    --with-icu
    --with-readline
    --with-threads
    --with-zlib=/usr/$(exhost --target)
    # We build python bindings separately to allow building
    # against multiple ABIs
    --without-python
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( ftp )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README.tests TODO_SCHEMAS )

src_unpack() {
    unpack ${PNV}.tar.xz

    # test suite
    unpack xmlts${TEST_VERSION}.tar
}

src_prepare() {
    default

    # prevent fetching with wget during src_test
    edo ln -s "${FETCHEDDIR}"/xsts-{${TAR1},${TAR2}}.tar.gz xstc/
}

src_install() {
    default

    # Remove libtool archive, they are all a PITA but this one in particular because libxml2 links
    # against icu and causes consumers to link against it as well, requiring rebuilds with new icu
    # slots
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/libxml2.la

    # devhelp doesn't support out-of-source builds
    edo cp -pPR "${WORK}"/doc/devhelp "${IMAGE}"/usr/share/doc/${PNVR}/html/

    if ! option doc; then
        edo rm -r "${IMAGE}"/usr/share/gtk-doc
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/html
    fi

    if ! option examples; then
        edo rm -r "${IMAGE}"/usr/share/doc/${PNVR}/examples
    fi
}

