# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Provide some BSD functions on GNU"
DESCRIPTION="
This library provides useful functions commonly found on BSD systems, and
lacking on others like GNU systems, thus making it easier to port projects
with strong BSD origins, without needing to embed the same code over and over
again on each project.
"
HOMEPAGE="https://${PN}.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

LICENCES="BSD-2 BSD-3 BSD-4 as-is"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libmd
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )

src_prepare() {
    if [[ $(exhost --target) == *-musl* ]] ; then
        # Test fails to build with musl
        edo sed -e '/fpurge \\/d' -i test/Makefile.am
        autotools_src_prepare
    else
        default
    fi
}

