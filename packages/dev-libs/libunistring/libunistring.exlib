# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ]
require alternatives

export_exlib_phases src_install src_test

SUMMARY="Library for manipulating Unicode and C strings according to the Unicode standard"

LICENCES="|| ( GPL-2 LGPL-3 )"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/libunistring:0[<1.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )

libunistring_src_install() {
    local arch_dependent_alternatives=()
    local arch_independent_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives=(
        /usr/${host}/include/unicase.h  unicase-${SLOT}.h
        /usr/${host}/include/uniconv.h  uniconv-${SLOT}.h
        /usr/${host}/include/unictype.h unictype-${SLOT}.h
        /usr/${host}/include/unigbrk.h  unigbrk-${SLOT}.h
        /usr/${host}/include/unilbrk.h  unilbrk-${SLOT}.h
        /usr/${host}/include/uniname.h  uniname-${SLOT}.h
        /usr/${host}/include/uninorm.h  uninorm-${SLOT}.h
        /usr/${host}/include/unistdio.h unistdio-${SLOT}.h
        /usr/${host}/include/unistr.h   unistr-${SLOT}.h
        /usr/${host}/include/unitypes.h unitypes-${SLOT}.h
        /usr/${host}/include/uniwbrk.h  uniwbrk-${SLOT}.h
        /usr/${host}/include/uniwidth.h uniwidth-${SLOT}.h
        /usr/${host}/include/unistring  unistring-${SLOT}
        /usr/${host}/lib/${PN}.la       ${PN}-${SLOT}.la
        /usr/${host}/lib/${PN}.so       ${PN}-${SLOT}.so
    )

    arch_independent_alternatives=(
        /usr/share/info/${PN}.info ${PN}-${SLOT}.info
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${arch_independent_alternatives[@]}"
}

libunistring_src_test() {
    esandbox allow_net --bind "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    default

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net --bind "inet:127.0.0.1@80"
}

