# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ARMmbed tag=v${PV} ] cmake toolchain-funcs

SUMMARY="Cryptographic library for embedded systems"
HOMEPAGE+=" https://tls.mbed.org"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    havege [[ description = [
        Enable the HAVEGE random generator (not suitable for virtualized environments)
    ] ]]
"

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen[>=1.8.4]
            media-gfx/graphviz
        )
    build+run:
        sys-libs/zlib
    test:
        dev-lang/perl:*
        dev-lang/python:*[>=3]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DENABLE_PROGRAMS:BOOL=FALSE
    -DENABLE_ZLIB_SUPPORT:BOOL=TRUE
    -DINSTALL_MBEDTLS_HEADERS:BOOL=TRUE
    -DLINK_WITH_PTHREAD:BOOL=TRUE
    -DLINK_WITH_TRUSTED_STORAGE:BOOL=FALSE
    -DMBEDTLS_FATAL_WARNINGS:BOOL=FALSE
    -DUNSAFE_BUILD:BOOL=FALSE
    -DUSE_PKCS11_HELPER_LIBRARY:BOOL=FALSE
    -DUSE_SHARED_MBEDTLS_LIBRARY:BOOL=TRUE
    -DUSE_STATIC_MBEDTLS_LIBRARY:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTING:BOOL=TRUE -DENABLE_TESTING:BOOL=FALSE'
)

enable_mbedtls_option() {
    local myopt="$@"
    # check that config.h syntax is the same at version bump
    edo sed \
        -e "s://#define ${myopt}:#define ${myopt}:" \
        -i "${CMAKE_SOURCE}"/include/mbedtls/config.h
}

src_prepare() {
    enable_mbedtls_option MBEDTLS_MD4_C
    enable_mbedtls_option MBEDTLS_THREADING_C
    enable_mbedtls_option MBEDTLS_THREADING_PTHREAD
    enable_mbedtls_option MBEDTLS_ZLIB_SUPPORT
    option havege && enable_mbedtls_option MBEDTLS_HAVEGE_C
    cc-has-defined __SSE2__ && enable_mbedtls_option MBEDTLS_HAVE_SSE2

    cmake_src_prepare
}

src_compile() {
    default

    if option doc ; then
        emake apidoc
    fi
}

src_test() {
    LD_LIBRARY_PATH="${ECMAKE_BUILD_DIR}/library" \
        emake test
}

src_install() {
    cmake_src_install

    if option doc ; then
        docinto html
        dodoc -r "${CMAKE_SOURCE}"/apidoc/*
    fi
}

