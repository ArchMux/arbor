# Copyright 2010, 2011, 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 2.5 ] supported_automake=[ 1.16 1.15 1.13 1.11 1.10 ] ]
export_exlib_phases src_configure src_test

SUMMARY="Pink's Tracing Library"
DESCRIPTION="
pinktrace is a wrapper around ptrace(2) system call.
It provides a robust API for tracing processes.
Note this is a work in progress and the API is *not* stable.
"
HOMEPAGE="https://dev.exherbo.org/~alip/${PN}"
if ! ever is_scm ; then
    DOWNLOADS="https://dev.exherbo.org/distfiles/${PN}/${PNV}.tar.bz2"
fi

MYOPTIONS="doc
    pvm [[ description = [ Use process_vm_{r,w}*() rather than ptrace() to read process mem. ]
           presumed = true
    ]]
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-ipv6 --enable-installed-tests )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc doxygen' )

REMOTE_IDS="freecode:${PN}"

UPSTREAM_DOCUMENTATION="
    ${HOMEPAGE}/api/c      [[ lang = en description = [ API documentation ] ]]
    ${HOMEPAGE}/api/python [[ lang = en description = [ Python bindings API documentation ] ]]
    ${HOMEPAGE}/api/ruby   [[ lang = en description = [ Ruby bindings API documentation ] ]]
"

LICENCES="BSD-3"
SLOT="0"

pinktrace_src_configure() {
    default

    option pvm || edo sed -i -e '/^#define PINK_HAVE_PROCESS_VM_\(READ\|WRITE\)V/s/1/0/' pinktrace/system.h
}

pinktrace_src_test() {
    if ! esandbox check 2>/dev/null; then
        default
    else
        elog 'Not running tests due to ptrace limitations.'
        elog 'Run pinktrace-check after installation to run installed tests.'
    fi
}

