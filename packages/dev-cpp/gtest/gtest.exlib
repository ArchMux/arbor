# Copyright 2016 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 1.13.0 ; then
    require github [ user=google project=googletest tag=v${PV} ] cmake
else
    require github [ user=google project=googletest tag=release-${PV} ] cmake
fi

SUMMARY="Google's framework for writing C++ tests on a variety of platforms, based on the xUnit architecture"
DESCRIPTION="
It supports automatic test discovery, a rich set of assertions, user-defined assertions, death tests,
fatal and non-fatal failures, various options for running the tests, and XML test report generation.
"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    vim-syntax
"

DEPENDENCIES="
    build:
        dev-lang/python:*
    suggestion:
        vim-syntax? ( app-vim/googletest-syntax )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_GMOCK:BOOL=TRUE
    -Dgtest_disable_pthreads:BOOL=FALSE
    -Dgtest_hide_internal_symbols:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-Dgtest_build_tests=TRUE -Dgtest_build_tests=FALSE'
)

if ever at_least 1.13.0 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DGTEST_HAS_ABSL:BOOL=FALSE
    )
fi

