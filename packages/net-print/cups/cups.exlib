# Copyright 2008-2009 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cups-1.3.8-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require github [ user=OpenPrinting release=v${PV} pnv=${PNV}-source suffix=tar.gz ] \
    pam systemd-service gtk-icon-cache \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ] \
    openrc-service

export_exlib_phases src_prepare src_configure src_install src_test

SUMMARY="Common UNIX Printing System"
DESCRIPTION="A portable printing layer for UNIX®-based operating systems."

LICENCES="Apache-2.0"
SLOT="0"
LANGS="ca cs da de es fr it ja pt_BR ru zh_CN"
MYOPTIONS="
    acl [[ description = [ Support POSIX ACL when authenticating with the CUPS daemon ] ]]
    avahi [[ description = [ Enable DNS Service Discovery (DNS_SD) support (printer sharing using Avahi) ] ]]
    dbus
    debug
    kerberos
    pam
    systemd
    tcpd [[ description = [ Use TCP Wrappers to restrict access to cupsd ] ]]
    xinetd
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
    ( libc: musl )
    linguas: ( ${LANGS} )
"

# testsuite has lots of FAILs for now, will check into those next version
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        group/lpadmin
        dev-libs/libpaper
        dev-libs/libusb:1
        sys-libs/zlib
        acl? ( sys-apps/acl )
        avahi? ( net-dns/avahi[dns_sd] )
        dbus? ( sys-apps/dbus )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        !libc:musl? ( dev-libs/libxcrypt:= )
        pam? ( sys-libs/pam )
        providers:gnutls? ( dev-libs/gnutls )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
    run:
        xinetd? ( sys-apps/xinetd )
    post:
        net-print/cups-filters [[ description = [ Common filters required by most printer drivers on Linux ] ]]
    suggestion:
        sys-apps/colord [[ description = [ ICC profiles referenced by PPD files are registered utilizing colord ] ]]
"

WORK=${WORKBASE}/${PNV}

AT_M4DIR=( config-scripts )

cups_src_prepare() {
    # do not strip by default
    edo sed \
        -e 's:INSTALL_STRIP="-s":INSTALL_STRIP="":' \
        -i config-scripts/cups-compiler.m4

    # do not overwrite libdir if lib64 is found on the system and
    # don't use the library dir as server directory, since that'll
    # break multilib
    edo sed \
        -e 's#libdir=".*"#:#' \
        -e '/CUPS_SERVERBIN="/s:lib/cups:libexec/cups:' \
        -i config-scripts/cups-directories.m4

    # Disable silent rules
    edo sed \
        -e '/^\.SILENT:$/d' \
        -i Makedefs.in

    autotools_src_prepare
}

cups_src_configure() {
    local myconf=()

    for lang in ${LANGS}; do
        option "linguas:${lang}" && CUPSLANGS="${lang} ${CUPSLANGS}"
    done

    [[ -z "${CUPSLANGS}" ]] && CUPSLANGS=none

    myconf+=(
        --localstatedir=/var
        --enable-libpaper
        --enable-libusb
        --enable-raw-printing
        --disable-sanitizer
        --disable-unit-tests
        --with-container=none
        --with-cups-{group,user}=lp
        --with-docdir="/usr/share/doc/${PNVR}/handbook"
        --with-languages="${CUPSLANGS}"
        --with-rundir=/run/cups
        --with-system-groups="lpadmin sys root wheel"
        --with-tls=$(option providers:gnutls gnutls openssl)
        --with-optim=""
        --without-rcdir
        $(option_enable acl)
        $(option_enable dbus)
        $(option_enable debug debug)
        $(option_enable debug debug-guards)
        $(option_enable debug debug-printfs)
        $(option_enable kerberos gssapi)
        $(option_enable pam)
        $(option_enable tcpd tcp-wrappers)
        $(option_with avahi dnssd avahi)
        $(option_with systemd ondemand systemd)
        $(option_with systemd systemd ${SYSTEMDSYSTEMUNITDIR})
        $(option_with xinetd xinetd /etc/xinetd.d)
        ac_cv_path_AR="${AR}"
        DSOFLAGS="${CFLAGS}" LDFLAGS="${CFLAGS} ${LDFLAGS}"
    )

    # build system doesn't like --without and will create a 'no' directory
    option dbus && myconf+=( --with-dbusdir=/usr/share/dbus-1 )

    econf "${myconf[@]}"
}

cups_src_install() {
    emake BUILDROOT="${IMAGE}" install
    emagicdocs

    keepdir /usr/$(exhost --target)/libexec/cups/driver /usr/share/cups/{model,profiles} \
            /var/cache/cups/rss /var/log/cups /var/spool/cups/tmp

    keepdir /etc/cups/{ppd,ssl}

    # provide our own pam script
    if option pam ; then
        edo rm -f "${IMAGE}"/etc/pam.d/cups
        pamd_mimic_system cups auth auth account
    fi

    echo "ServerName /run/cups/cups.sock" >> "${IMAGE}"/etc/cups/client.conf

    # cups-filters now provides these
    edo rmdir "${IMAGE}"/usr/share/cups/{banners,data}

    # directories are created automatically on startup
    edo rm -r "${IMAGE}"/run

    # with [-systemd] the systemd files will be put in a 'no' directory that leads to:
    # !!! Not allowed to merge '/var/tmp/paludis/build/net-print-cups-2.0.2/image/no' to '/no'
    option systemd || edo rm -rf "${IMAGE}"/no

    install_openrc_files
}

cups_src_test() {
    # 1 = medium testsuite
    emake test
    edo cd test
    USER=paludisbuild edo ./run-stp-tests.sh 1 2 n
}

#cups_src_test_expensive() {
#    if exhost --is-native ; then
#        # 4 = comprehensive testsuite
#        emake test
#        edo cd test
#        USER=paludisbuild edo ./run-stp-tests.sh 4 2 n
#    fi
#}

