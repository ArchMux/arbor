# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A library for making GPG easier to use"
HOMEPAGE="https://www.gnupg.org/related_software/gpgme"
DOWNLOADS="mirror://gnupg/gpgme/gpgme-${PV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    doc [[ description = [ Build API docs for the Qt bindings ] ]]
"

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            media-gfx/graphviz
        )
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=${PV}]
        app-crypt/gnupg[>=2.0.4]
        dev-libs/libassuan[>=2.4.2]
        dev-libs/libgpg-error[>=1.36]
        x11-libs/qtbase:5
        !app-crypt/gpgme[qt5] [[
            description = [ libqgpgme was split out of app-crypt/gpgme ]
            resolution = uninstall-blocked-after
        ]]
"

WORK="${WORKBASE}"/gpgme-${PV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # qt bindings depend on cpp
    --enable-languages="cpp,qt"
    --disable-gpg-test
    --disable-gpgsm-test
    --with-libassuan-prefix=/usr/$(exhost --target)
    --with-libgpg-error-prefix=/usr/$(exhost --target)
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "doc ac_cv_prog_DOXYGEN=doxygen ac_cv_prog_DOXYGEN="
)

DEFAULT_SRC_COMPILE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
)

src_test() {
    # Allow the tests to run its own instance of gpg-agent
    esandbox allow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox allow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"

    esandbox allow_net "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
    esandbox allow_net --connect "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
    esandbox allow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
    esandbox allow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
    esandbox allow_net "unix:${WORK}/lang/qt/tests/S.dirmngr*"
    esandbox allow_net --connect "unix:${WORK}/lang/qt/tests/S.dirmngr*"

    default

    esandbox disallow_net --connect "unix:${WORK}/lang/qt/tests/S.dirmngr*"
    esandbox disallow_net "unix:${WORK}/lang/qt/tests/S.dirmngr*"
    esandbox disallow_net --connect "unix:${TEMP}/t-various-*/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/t-various-*/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/t-tofuinfo-*/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${WORK}/lang/qt/tests/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/lang/qt/tests/S.gpg-agent*"

    esandbox disallow_net --connect "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net "unix:${WORK}/tests/gpg*/S.dirmngr*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpgsm/S.gpg-agent*"
    esandbox disallow_net --connect "unix:${WORK}/tests/gpg/S.gpg-agent*"
    esandbox disallow_net "unix:${WORK}/tests/gpg/S.gpg-agent*"
}

src_install() {
    default

    local host=$(exhost --target)
    # Remove the parts we already have from app-crypt/gpgme
    edo pushd "${IMAGE}"/usr/
    edo rm -r ${host}/include/gpgme++ \
            ${host}/lib/cmake/Gpgmepp
    edo rm ${host}/bin/gpgme-* \
        ${host}/include/gpgme.h \
        ${host}/lib/{libgpgme.*,libgpgmepp.*} \
        ${host}/lib/pkgconfig/gpgme*.pc
    edo rmdir ${host}/{bin,lib/pkgconfig}

    edo rm share/aclocal/gpgme.m4 share/info/gpgme.info*
    edo rmdir share/aclocal
    edo popd

    if option doc ; then
        edo pushd lang/qt/doc/generated
        dodoc -r html
        edo popd
    fi
}

