Upstream: yes, taken from release/2.36/master

From dd4131c8322891a0ad7cfb661efa41aecc02b581 Mon Sep 17 00:00:00 2001
From: Aurelien Jarno <aurelien@aurel32.net>
Date: Tue, 1 Nov 2022 20:43:55 +0100
Subject: [PATCH 68/69] linux: Fix fstatat on MIPSn64 (BZ #29730)

Commit 6e8a0aac2f883 ("time: Fix overflow itimer tests on 32-bit
systems") changed in_time_t_range to assume a 32-bit time_t. This broke
fstatat on MIPSn64 that was using it with a 64-bit time_t due to
difference between stat and stat64. This commit fix that by adding a
MIPSn64 specific version, which bypasses the EOVERFLOW tests.

Resolves: BZ #29730

Reviewed-by: Adhemerval Zanella  <adhemerval.zanella@linaro.org>
(cherry picked from commit 7457b7eef8dfe8cc48e55b9f9837df6dd397b80d)
---
 NEWS                                          |  1 +
 .../unix/sysv/linux/mips/mips64/n64/fstatat.c | 51 +++++++++++++++++++
 2 files changed, 52 insertions(+)
 create mode 100644 sysdeps/unix/sysv/linux/mips/mips64/n64/fstatat.c

diff --git a/NEWS b/NEWS
index 833045585f..e92d547e2c 100644
--- a/NEWS
+++ b/NEWS
@@ -45,6 +45,7 @@ The following bugs are resolved with this release:
   [29638] libc: stdlib: arc4random fallback is never used
   [29657] libc: Incorrect struct stat for 64-bit time on linux/generic
     platforms
+  [29730] broken y2038 support in fstatat on MIPS N64
 
 Version 2.36
 
diff --git a/sysdeps/unix/sysv/linux/mips/mips64/n64/fstatat.c b/sysdeps/unix/sysv/linux/mips/mips64/n64/fstatat.c
new file mode 100644
index 0000000000..fe6c3a0dda
--- /dev/null
+++ b/sysdeps/unix/sysv/linux/mips/mips64/n64/fstatat.c
@@ -0,0 +1,51 @@
+/* Get file status.  Linux/MIPSn64 version.
+   Copyright (C) 2022 Free Software Foundation, Inc.
+   This file is part of the GNU C Library.
+
+   The GNU C Library is free software; you can redistribute it and/or
+   modify it under the terms of the GNU Lesser General Public
+   License as published by the Free Software Foundation; either
+   version 2.1 of the License, or (at your option) any later version.
+
+   The GNU C Library is distributed in the hope that it will be useful,
+   but WITHOUT ANY WARRANTY; without even the implied warranty of
+   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+   Lesser General Public License for more details.
+
+   You should have received a copy of the GNU Lesser General Public
+   License along with the GNU C Library; if not, see
+   <https://www.gnu.org/licenses/>.  */
+
+#include <sys/stat.h>
+#include <sysdep.h>
+
+/* Different than other ABIs, mips64 has different layouts for non-LFS
+   and LFS struct stat.  */
+int
+__fstatat (int fd, const char *file, struct stat *buf, int flag)
+{
+  struct __stat64_t64 st64;
+  int r = __fstatat64_time64 (fd, file, &st64, flag);
+  if (r == 0)
+    {
+      /* Clear internal pad and reserved fields.  */
+      memset (buf, 0, sizeof (*buf));
+
+      buf->st_dev = st64.st_dev;
+      buf->st_ino = st64.st_ino;
+      buf->st_mode = st64.st_mode;
+      buf->st_nlink = st64.st_nlink;
+      buf->st_uid = st64.st_uid;
+      buf->st_gid = st64.st_gid;
+      buf->st_rdev = st64.st_rdev;
+      buf->st_size = st64.st_size;
+      buf->st_blksize = st64.st_blksize;
+      buf->st_blocks  = st64.st_blocks;
+      buf->st_atim = st64.st_atim;
+      buf->st_mtim = st64.st_mtim;
+      buf->st_ctim = st64.st_ctim;
+    }
+  return r;
+}
+
+weak_alias (__fstatat, fstatat)
-- 
2.38.1

