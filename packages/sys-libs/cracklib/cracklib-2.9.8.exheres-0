# Copyright 2007 Bryan Østergaard <kloeri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.bz2 ] \
    python [ blacklist='2' multibuild=false with_opt=true ]

# Update autotools to make python macro recognise Python versions with double digit minor
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A proactive password sanity library"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( linguas: as be bn_IN ca cs da de el es eu fi fr gu he hi hu id it ja kk kn ko lt ml mr nb nl
               or pa pl pt pt_BR ru sk sl sl_SI sq sv ta te tr uk zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.17]
    build+run:
        sys-libs/zlib
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --disable-static
    --with-default-dict=/var/cache/cracklib/cracklib_dict
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( python )

src_install() {
    default

    option python && python_bytecompile

    dodir /var/cache/cracklib
    keepdir /var/cache/cracklib

    dodir /usr/share/dict
    insinto /usr/share/dict
    doins dicts/cracklib-small
}

pkg_postinst() {
    edo "/usr/$(exhost --build)/bin/cracklib-format" "${ROOT:-/}usr/share/dict/cracklib-small" \
        | "/usr/$(exhost --build)/bin/cracklib-packer" "${ROOT:-/}var/cache/${PN}/cracklib_dict"
}

