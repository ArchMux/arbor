# Copyright 2008-2015 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="Utilities for ELF binaries (e. g. scanelf)"
HOMEPAGE="https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities"
DOWNLOADS="mirror://gentoo/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    caps
    seccomp [[ description = [ System call filtering support via seccomp ] ]]
"

DEPENDENCIES="
    build:
        caps? ( virtual/pkg-config )
    build+run:
        caps? ( sys-libs/libcap )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuild_manpages=disabled
    -Dlddtree_implementation=sh
    -Duse_fuzzing=false
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'caps use_libcap'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'seccomp use_seccomp'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

src_prepare() {
    meson_src_prepare

    # py script is not compatible with cross
    # we do not install it so don't test it
    edo sed \
        -e "s:, 'py', 'cmp'::g" \
        -i tests/lddtree/meson.build
}

