# Copyright 2008, 2009, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008 Santiago Mola <coldwind@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'tcl-8.5.3.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

MY_PNV="${PN}${PV}"

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require sourceforge [ suffix=tar.gz pnv=${MY_PNV}-src ]

SUMMARY="Tool Command Language"
DESCRIPTION="
Tcl (Tool Command Language) is a very powerful but easy to learn dynamic
programming language, suitable for a very wide range of uses, including web and
desktop applications, networking, administration, testing and many more. Open
source and business-friendly, Tcl is a mature yet evolving language that is
truly cross platform, easily deployed and highly extensible.
"
HOMEPAGE+=" https://www.tcl-lang.org/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="debug"

DEPENDENCIES=""

WORK="${WORKBASE}/${MY_PNV}/unix"

RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p2 "${FILES}"/${PN}-tclm4-soname.patch
    -p2 "${FILES}"/${PN}-tclConfig-paths.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    # We consider them as arch-dependent
    TCL_LIBRARY='$(exec_prefix)/lib/tcl$(VERSION)'
)

DEFAULT_SRC_INSTALL_PARAMS=(
    # We consider them as arch-dependent
    TCL_LIBRARY='$(exec_prefix)/lib/tcl$(VERSION)'
    install-private-headers
)

src_prepare() {
    # Exclude bundled packages (itcl, thread, tdbc). They aren't used by anything
    # but require more build system patches to work.
    edo sed \
        -e "/^all:/ s/packages//" \
        -e "/^INSTALL_PACKAGE_TARGETS =/ d" \
        -i Makefile.in

    autotools_src_prepare

    # FIXME: WORK points to unix/, we want to patch ../generic.
    edo pushd ..
    expatch -p0 "${FILES}"/${PN}-stat64.patch
    edo popd
}

src_configure() {
    local extra_params=( )

    if ! exhost --is-native -q ; then
        extra_params+=(
            # NOTE(tridactyla) When cross-compiling, tcl conservatively assumes that the
            # implementations of these functions are buggy because it cannot test that they are not.
            # Due to a bug in tcl (http://core.tcl.tk/tcl/tktview?name=1195167fff), this causes a
            # compilation failure because of duplicate fixstrod symbols. Instead, just assume that
            # they work correctly.
            tcl_cv_strstr_unbroken=ok
            tcl_cv_strtoul_unbroken=ok
            tcl_cv_strtod_unbroken=ok
            tcl_cv_strtod_buggy=ok
        )
    fi

    econf \
        $(option_enable debug symbols) "${extra_params[@]}"
}

src_install() {
    local host=$(exhost --target)

    default

    local v1=$(ever range 1-2)
    dosym tclsh${v1} /usr/${host}/bin/tclsh

    dodoc "${WORKBASE}"/${MY_PNV}/{ChangeLog*,changes,README.md}
}

